<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи и ABSPATH. Дополнительную информацию можно найти на странице
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется скриптом для создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения вручную.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'flush');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'root');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pZ`S&M#@zxqR;|ua{*{dl+4}H)&QH?Xr(`x!g7zVdSJd&l-ba:{,fHJnV:YlRC_r');
define('SECURE_AUTH_KEY',  'TGQBwUp=!>gy&:B9?`8XU!YW a}/((0mkp~Sb_mrwaK|KVBNedFni[>M2EmbFhrR');
define('LOGGED_IN_KEY',    'D{[JLvUXV}2|bC?7i2p j.>&D4f$s|H5 @J!}KS`]qB0F1CC~N1Z$:v%C:[S-|u-');
define('NONCE_KEY',        '?TDDUEdJd@[Yklm,,@bRw@)z.xr;/A75^fvquf7`vj.CBC--R.f`l|wK+O-K>PQx');
define('AUTH_SALT',        '(YI!8DO!}Jv,4u5,d $Xz60+RUDkiXDb>|s8k;|/CNg`X-/i2@vW4x0|BQg~-cEI');
define('SECURE_AUTH_SALT', '~?r~~4p-3T /,0,kHD7Z[J.D6+N~i5-;U5:p0+mp|lwkd_GZ2#5oCBz#HOfg||m[');
define('LOGGED_IN_SALT',   '@]uGhNK/+Gx7YpQ%VqnOA5j=S-agB8,y+3R5ld%+FN}4.xzAYmo $JuK+*{q-iN,');
define('NONCE_SALT',       's)cnb2qY=j(Z;9`Tbh&~q?0&q#y:p4bxh!uz~HGhqv8e--W?[B:h]ce=z-|`^wE_');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
