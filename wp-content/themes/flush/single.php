<?php
/**
 * The Template for displaying all single posts.
 */
global $theme_data;
//social
$facebook = $theme_data['facebook'];
$twitter = $theme_data['twitter'];
$instagram = $theme_data['instagram'];
//logo
$logo = $theme_data['logo'];
//backgrounds
$bgFirst = $theme_data['bg_first'];
$bgSecond = $theme_data['bg_second'];
//mail
$mail = $theme_data['mail'];
//video
$video = $theme_data['video_link'];
$videoBox = $theme_data['video_box'];

get_header(); ?>
	<div class="spanning">
        <div class="logo-box lb-item" style="background-image: url(<?php echo $bgFirst; ?>);">
            <!-- <video id="video" width="100%" height="auto" autoplay="autoplay" loop="loop" preload="auto">
                <source src="<?php echo $videoBox; ?>"></source>
            </video> -->
            <div class="logo-item-box">
                <div class="lib">
                    <div class="logo">
                        <a href="/"><img src="<?php echo $logo; ?>" alt=""></a>
                    </div><!-- /.logo -->
                </div><!-- /.lib -->
            </div><!-- /.logo-item-box -->
        </div><!-- /.logo-box -->
		<div class="wrap">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?> 
			<article class="text">
				<div class="date"><?php the_time('d / M / Y');?></div>
				<h1><?php the_title();?></h1>
				<?php the_content();?>
			</article><!-- /.text -->
			<?php endwhile;?>
			<div class="pagination clearfix">
				<span class="prev-link"><?php previous_post_link('%link', '<span class="fa fa-angle-left"></span> previous post', TRUE); ?></span>
				<span class="next-link"><?php next_post_link('%link', '<span class="fa fa-angle-right"></span> next post', TRUE); ?></span>
			</div><!-- /.pagination -->
			<div class="comment">
				<h2>Leave a Comment</h2>
				<?php echo do_shortcode( '[vivafbcomment count="off" num="6" countmsg="awesome comments"]' ); ?>
				
			</div><!-- /.comment -->
		</div><!-- /.wrap -->
	</div><!-- /.spanning -->
</div><!-- /.main -->
<?php get_footer(); ?>
