<?php
/*----------------------------------------/
	THEME VARIABLES
/----------------------------------------*/

/*----------------------------------------/
	DEFINE PATHS
/----------------------------------------*/
define('ADMIN_PATH', TEMPLATEPATH . '/library/admin/');
define( 'ADMIN_DIR', get_template_directory_uri() . '/library/admin/' );
define('OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/library/admin/');

//------ THEME OPTIONS PANEL ------//
require_once (TEMPLATEPATH . '/library/admin/index.php');
if ( ! isset( $content_width ) )
	$content_width = 640;
add_action( 'after_setup_theme', 'flush_setup' );

if ( ! function_exists( 'flush_setup' ) ):
function flush_setup() {

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'flush' ),
	) );
	
}
endif;

function remove_customize_page(){
    global $submenu;
    unset($submenu['themes.php'][6]); // remove customize link
}
add_action( 'admin_menu', 'remove_customize_page');


//remove link
update_option('image_default_link_type', 'none' );

function create_post_type() {
    register_post_type('partner',
        array(
            'labels'         => array(
                'name'          => __('Partners'),
                'singular_name' => __('partner')
            ),
            'public'      => true,
            'has_archive' => true,
        )
    );
    pti_set_post_type_icon( 'partner', 'picture-o' );
    flush_rewrite_rules();
    register_post_type('about',
        array(
            'labels'         => array(
                'name'          => __('About'),
                'singular_name' => __('about')
            ),
            'public'      => true,
            'has_archive' => true,
        )
    );
    pti_set_post_type_icon( 'about', 'video-camera' );
    flush_rewrite_rules();
    register_post_type('issue',
        array(
            'labels'         => array(
                'name'          => __('Issue'),
                'singular_name' => __('issue')
            ),
            'public'      => true,
            'has_archive' => true,
        )
    );
    pti_set_post_type_icon( 'issue', 'user-md' );
    flush_rewrite_rules();
    register_post_type('support',
        array(
            'labels'         => array(
                'name'          => __('Support'),
                'singular_name' => __('support')
            ),
            'public'      => true,
            'has_archive' => true,
        )
    );
    pti_set_post_type_icon( 'support', 'medkit' );
    flush_rewrite_rules();
    register_post_type('team',
        array(
            'labels'         => array(
                'name'          => __('Team'),
                'singular_name' => __('team')
            ),
            'public'      => true,
            'has_archive' => true,
        )
    );
    pti_set_post_type_icon( 'team', 'users' );
    flush_rewrite_rules();
    register_post_type('donate',
        array(
            'labels'         => array(
                'name'          => __('Donate'),
                'singular_name' => __('donate')
            ),
            'public'      => true,
            'has_archive' => true,
        )
    );
    pti_set_post_type_icon( 'donate', 'money' );
    flush_rewrite_rules();

}
add_action('init', 'create_post_type');
