$(document).ready(function(){

    //nojs
    $("body").removeClass("no-js");

    //------------------------------------------------------------------------//

    //fakelink
    $('a[href="#"]').on('click',function(e){e.preventDefault();});

    //------------------------------------------------------------------------//

    //doubleTapToGo
    $("nav > li:has(ul)").doubleTapToGo();
    $("nav li:first a").wrapInner('<span></span>');
    $("nav li:last").addClass('last-menu');
    $('.open-menu').on('click',function(){
        $(this).toggleClass('active');
        $('nav').toggleClass('active');
        return false;
    });  
    $('a[href="#chimpy"]').each(function(){
      $(this).attr('id','chimpy_popup_open');
    });

    //------------------------------------------------------------------------//
    
    //scroll
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
      });

    //------------------------------------------------------------------------//

    //placeholder
    $('input[placeholder], textarea[placeholder]').placeholder();

    //------------------------------------------------------------------------//

    $('.partners').slick({
      dots: false,
      speed: 300,
      infinite: true,
      slidesToShow: 8,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1300,
          settings: {
            slidesToShow: 7,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
    $('.ss-slider').slick({
      dots: false,
      speed: 300,
      infinite: true,
      slidesToShow: 6,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1500,
          settings: {
            slidesToShow: 6,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 1300,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });

    //------------------------------------------------------------------------//

    //clear
    $(".people:nth-child(2n)").after("<div class='clear'></div>");

    //------------------------------------------------------------------------//

    //show video
    $('.show-video').on('click',function(){
      $('.promo iframe').show();
    })

    //------------------------------------------------------------------------//

    //video box
    var fullHeight = function(){
        var bodyHeight = $('body').innerHeight();
        $('.logo-box,.logo-item-box,.lib').css({
            height: bodyHeight
        });
    }
    fullHeight();
    $(window).on('resize', function(event) {
        fullHeight();
    });

    $('.menu-item-57 a').on('click',function(ev){
      $("#video-iframe")[0].src += "&autoplay=1";
      $('#video-iframe').fadeIn(); 
      ev.preventDefault();
    });
    $('.play-video-link').on('click',function(ev){
      $("#video-iframe")[0].src += "&autoplay=1";
      $('#video-iframe').fadeIn(); 
      ev.preventDefault();
    });
    //------------------------------------------------------------------------//

    //popup
    // $(window).bind("touchstart click", function(e){
    //     if (('.popup').length) {
    //         $('.popup').hide();
    //         $('html').removeClass('popup-hide');
    //     }
    // });
    // $('.popup').bind("click touchstart", function(event){
    //     event.stopPropagation();
    // });
    // $(document).keydown(function(e){
    //     if (e.keyCode == 27){
    //         $('.popup').hide();
    //         $('html').removeClass('popup-hide');
    //         e.preventDefault();
    //     }
    // });
    // $('.last-menu a').on('click',function(){
    //     if (('.popup').length) {
    //         $('.popup').show();
    //         $('html').addClass('popup-hide');
    //     }
    //     return false;
    // });
    // $('.close-popup').on('click',function(){
    //     if (('.popup').length) {
    //         $('.popup').hide();
    //         $('html').removeClass('popup-hide');
    //     }
    //     return false;
    // });

    //------------------------------------------------------------------------//


    
});//document ready

