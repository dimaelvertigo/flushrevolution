<?php
/**
 * The main template file.
 */
//general
global $theme_data;
//social
$facebook = $theme_data['facebook'];
$twitter = $theme_data['twitter'];
$instagram = $theme_data['instagram'];
//logo
$logo = $theme_data['logo'];
//backgrounds
$bgFirst = $theme_data['bg_first'];
$bgSecond = $theme_data['bg_second'];
//mail
$mail = $theme_data['mail'];
//video
$video = $theme_data['video_link'];
$videoBox = $theme_data['video_box'];

get_header(); ?>

        <div class="spanning">
            <div id="logo-box" class="logo-box" style="background-image: url(<?php echo $bgFirst; ?>);">

               <!--  <video id="video" width="100%" height="auto" autoplay="autoplay" loop="loop" preload="auto">
                    <source src="<?php echo $videoBox; ?>"></source>
                </video> -->



                <div class="logo-item-box">
                    <div class="lib">
                        <div class="logo">
                            <img src="<?php echo $logo; ?>" alt="">
                        </div><!-- /.logo -->
                        <a href="https://www.kickstarter.com/projects/937258398/flush-revolution" id="play-button" class="play-video-link">watch promo</a>
                        <a href="#popup" class="lib-donate">donate</a>
                        <div class="social-logo"> 
                            <?php if ($facebook) { ?><a href="<?php echo $facebook; ?>" class="fa fa-facebook"></a><?php } ?>
                            <?php if ($twitter) { ?><a href="<?php echo $twitter; ?>" class="fa fa-twitter"></a><?php } ?>
                            <?php if ($instagram) { ?><a href="<?php echo $instagram; ?>" class="fa fa-instagram"></a><?php } ?>
                        </div><!-- /.social-logo -->
                    </div><!-- /.lib -->
                </div><!-- /.logo-item-box -->
                <!-- <iframe src="<?php echo $video; ?>?rel=0" frameborder="0"  id="video-iframe" allowfullscreen></iframe>-->

                <iframe src="https://www.youtube.com/embed/QQBgGHMgIcI?rel=0" frameborder="0" allowfullscreen id="video-iframe"></iframe>
            </div><!-- /.logo-box -->
           

            <?php query_posts('showposts=1&post_type=about'); ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="about" id="about">
                <h2><?php the_title();?></h2>
                <? if (get_field('additional_title')): ?><h1><?php the_field('additional_title'); ?></h1><? endif; ?>
                <div class="about-text">
                    <?php the_content();?>
                </div><!-- /.about-text -->
            </div><!-- /.about -->
            <?php endwhile; ?>
            <?php wp_reset_query(); ?>  
            <div class="separator"><span></span></div>
            <?php query_posts('showposts=1&post_type=issue'); ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="issue" id="issue">
                <div class="page-title">
                    <h2><?php the_title();?></h2>
                    <? if (get_field('additional_title')): ?><h3><?php the_field('additional_title'); ?></h3><? endif; ?>
			<h3><b>The sanitation situation is not just gross or inconvenient - it's a matter of life and death</b></h3>
            <ul class="issue-content">
                <li>
                    <img src="<?php bloginfo('template_url') ?>/images/PM-1.png" alt="">
                    <p>
                        With no safe place to go, 4 out of 10 people are forced to face the dangers of public defecation
                    </p>
                </li>
                <li>
                    <img src="<?php bloginfo('template_url') ?>/images/PM.png" alt="">
                    <p>
                        1 of 3 women do not have a safe place to relieve themselves. They risk regular sexual harassment and rape
                    </p>
                </li>
                <li>
                    <img src="<?php bloginfo('template_url') ?>/images/PM-2.png" alt="">
                    <p>
                        A child dies of water born diseases about every 15 seconds.
                    By this time tomorrow, another 2,500 will be dead.
                    </p>
                </li>
            </ul>
<br><br>
<h3> <b>Worldwide more people have cell phones than a toilet. 
    Here are some sanitation soldiers on a mission to change that:</b> <br><br>
</h3>
                    </div>
         </div><!-- /.page-title -->
                <div class="wrap">
                    <?php if( have_rows('people_repeater') ): ?>
                    <?php while( have_rows('people_repeater') ): the_row(); 
                        $imagePeople = get_sub_field('issue_image');
                        $titlePeople = get_sub_field('title_name');
                        $blockquotePeople = get_sub_field('blockquote_issue');
                        $textPeople = get_sub_field('description_issue');
                    ?>
                    <div class="people">
                        <img src="<?php echo $imagePeople['url']; ?>" width="200" height="193" alt="<?php echo $imagePeople['alt'] ?>">
                        <div class="people-text">
                            <h2><?php echo $titlePeople; ?></h2>
                            <h3><?php echo $blockquotePeople; ?></h3>
                            <p><?php echo $textPeople; ?></p>
                        </div><!-- /.people-text -->
                    </div><!-- /.people -->
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div><!-- /.wrap -->
            </div><!-- /.issue -->
            <?php endwhile; ?>
            <?php wp_reset_query(); ?> 
            <?php query_posts('showposts=1&post_type=support'); ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="separator"><span></span></div>
            <div class="support" id="support">
                <div class="page-title">
                    <h2><?php the_title();?></h2>
                    <? if (get_field('additional_title')): ?><h3><?php the_field('additional_title'); ?></h3><? endif; ?>
                </div><!-- /.page-title -->
                <div class="support-text">
                    <?php the_content();?>
                </div><!-- /.support-text -->
                <? if (get_field('links_and_image')): ?>
                <div class="support-bottom">
                    <?php the_field('links_and_image'); ?>
                </div><!-- /.support-bottom -->
                <? endif; ?>
                <div class="support-sponsors">
                    <h2>Supporting Organizations</h2>
                    <div class="ss-slider">
                        <?php if( have_rows('supporting_organizations') ): ?>
                        <?php while( have_rows('supporting_organizations') ): the_row(); 
                            $imageSupport = get_sub_field('so_upload_image');
                        ?>
                        <div class="ss-item">
                            <img src="<?php echo $imageSupport['url']; ?>" alt="<?php echo $imageSupport['alt'] ?>">
                        </div><!-- /.ss-item -->
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div><!-- /.ss-slider -->
                </div><!-- /.support-sponsors -->
            </div><!-- /.support -->
            <?php endwhile; ?>
            <?php wp_reset_query(); ?> 
            <?php query_posts('showposts=1&post_type=donate'); ?>
            <?php while (have_posts()) : the_post(); ?>
       
            <?php endwhile; ?>
            <?php wp_reset_query(); ?> 
            <div class="separator"><span></span></div>
            <?php query_posts('showposts=1&post_type=team'); ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="team" id="team">
                <div class="page-title">
                    <h2><?php the_title();?></h2>
                </div><!-- /.page-title -->
                <div class="wrap">
                    <?php if( have_rows('people_repeater') ): ?>
                    <?php while( have_rows('people_repeater') ): the_row(); 
                        $imagePeople = get_sub_field('issue_image');
                        $titlePeople = get_sub_field('title_name');
                        $blockquotePeople = get_sub_field('blockquote_issue');
                        $textPeople = get_sub_field('description_issue');
                    ?>
                    <div class="people">
                        <img src="<?php echo $imagePeople['url']; ?>" width="200" height="193" alt="<?php echo $imagePeople['alt'] ?>">
                        <div class="people-text">
                            <h2><?php echo $titlePeople; ?></h2>
                            <h3><?php echo $blockquotePeople; ?></h3>
                            <p><?php echo $textPeople; ?></p>
                        </div><!-- /.people-text -->
                    </div><!-- /.people -->
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div><!-- /.wrap -->
            </div><!-- /.team -->
            <?php endwhile; ?>
            <?php wp_reset_query(); ?> 
            <div class="separator"><span></span></div>
            <div class="talk" id="talk">
                <div class="page-title">
                    <h2>Sh*t Talk</h2>
                </div><!-- /.page-title -->
                <?php query_posts('showposts=2&cat=1'); ?>
                <?php while (have_posts()) : the_post(); ?> 
                <div class="talk-box tb-post">
                    <? if (get_field('blog_image')): ?><a href="<?php the_permalink(); ?>"><img src="<?php the_field('blog_image'); ?>" alt=""></a><? endif; ?>
                    <p><?php the_time('d / M / Y');?></p>
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h2>
                    <span><?php echo wp_trim_words( get_the_content(), 20 ); ?></span>
                    <a href="<?php the_permalink(); ?>" class="read-more">Read more...</a>
                </div><!-- /.talk-box -->   
                <?php endwhile; ?>
                <?php wp_reset_query(); ?>             
                <!--div class="talk-box tb-tweets">
                tweets
                </div>/.talk-box -->
            </div><!-- /.talk -->
            <div class="separator"><span></span></div>
            <div class="contact" id="contact">
                <div class="send-mail">
                    <span class="fa fa-envelope-o"></span>
                    <h2>contact the filmmakers</h2>
                    <a href="mailto:<?php echo $mail; ?>" class="mailto-first"><?php echo $mail; ?></a>
                    <a href="mailto:<?php echo $mail; ?>" class="mailto-second">SEND MAIL</a>
                </div><!-- /.send-mail -->
                <div class="contact-separator"></div>
                <div class="subscribe">
                    <span class="fa fa-rss"></span>
                    <h2>Send <br> Me Shit</h2>
                    <!-- Begin MailChimp Signup Form -->
                    <div id="mc_embed_signup">
                    <form action="//flushrevolution.us10.list-manage.com/subscribe/post?u=f7df30242cdd62e8a648e8aa4&amp;id=ddeb76cb1b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">
                        
                    <div class="mc-field-group">
                        <input type="email" placeholder="Enter your E-mail" value="" name="EMAIL" class="input-text required email" id="mce-EMAIL">
                        <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="button">
                            <i class="fa fa-envelope-o"></i>
                        </button>
                    </div>
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;"><input type="text" name="b_f7df30242cdd62e8a648e8aa4_ddeb76cb1b" tabindex="-1" value=""></div>
                        </div>
                    </form>
                    </div>

                    <!--End mc_embed_signup-->
                </div><!-- /.subscribe -->
            </div><!-- /.contact -->
        </div><!-- /.spanning -->
    </div><!-- /.main -->
<?php get_footer(); ?>