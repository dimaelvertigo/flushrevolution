<?php
add_action('init','of_options');
if (!function_exists('of_options'))
{
	function of_options()
	{
		//Access the WordPress Categories via an Array
		$of_categories 		= array();  
		$of_categories_obj 	= get_categories('hide_empty=0');
		foreach ($of_categories_obj as $of_cat) {
		    $of_categories[$of_cat->cat_ID] = $of_cat->cat_name;}
		$categories_tmp 	= array_unshift($of_categories, "Select a category:");    
	       
		//Access the WordPress Pages via an Array
		$tt_pages = array();
		$tt_pages_obj = get_pages('sort_column=post_parent,menu_order');    
		foreach ($tt_pages_obj as $tt_page) {
			$tt_pages[$tt_page->ID] = $tt_page->post_title;
		}
		$tt_pages = array_flip($tt_pages);
		$tt_pages_tmp = array_unshift($tt_pages, "0"); 
		unset($tt_pages["0"]);
		$tt_pages = array_flip($tt_pages);
	
		//Testing 
		$of_options_select 	= array("one","two","three","four","five"); 
		$of_options_radio 	= array("one" => "One","two" => "Two","three" => "Three","four" => "Four","five" => "Five");
		
		//Sample Homepage blocks for the layout manager (sorter)
		$of_options_homepage_blocks = array
		( 
			"disabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_one"		=> "Block One",
				"block_two"		=> "Block Two",
				"block_three"	=> "Block Three",
			), 
			"enabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_four"	=> "Block Four",
			),
		);


		//Stylesheets Reader
		$alt_stylesheet_path = LAYOUT_PATH;
		$alt_stylesheets = array();
		
		if ( is_dir($alt_stylesheet_path) ) 
		{
		    if ($alt_stylesheet_dir = opendir($alt_stylesheet_path) ) 
		    { 
		        while ( ($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false ) 
		        {
		            if(stristr($alt_stylesheet_file, ".css") !== false)
		            {
		                $alt_stylesheets[] = $alt_stylesheet_file;
		            }
		        }    
		    }
		}


		//Background Images Reader
		/*$bg_images_path = get_stylesheet_directory(). '/images/bg/'; // change this to where you store your bg images
		$bg_images_url = get_template_directory_uri().'/images/bg/'; // change this to where you store your bg images
		$bg_images = array();
		
		if ( is_dir($bg_images_path) ) {
		    if ($bg_images_dir = opendir($bg_images_path) ) { 
		        while ( ($bg_images_file = readdir($bg_images_dir)) !== false ) {
		            if(stristr($bg_images_file, ".png") !== false || stristr($bg_images_file, ".jpg") !== false) {
		            	natsort($bg_images); //Sorts the array into a natural order
		                $bg_images[] = $bg_images_url . $bg_images_file;
		            }
		        }    
		    }
		}*/
		

		/*-----------------------------------------------------------------------------------*/
		/* TO DO: Add options/functions that use these */
		/*-----------------------------------------------------------------------------------*/
		
		//More Options
		$uploads_arr 		= wp_upload_dir();
		$all_uploads_path 	= $uploads_arr['path'];
		$all_uploads 		= get_option('of_uploads');
		$other_entries 		= array("Select a number:","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19");
		$body_repeat 		= array("no-repeat","repeat-x","repeat-y","repeat");
		$body_pos 			= array("top left","top center","top right","center left","center center","center right","bottom left","bottom center","bottom right");
		
		// Image Alignment radio box
		$of_options_thumb_align = array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center"); 
		
		// Image Links to Options
		$of_options_image_link_to = array("image" => "The Image","post" => "The Post"); 


/*-----------------------------------------------------------------------------------*/
/* The Options Array */
/*-----------------------------------------------------------------------------------*/

// Set the Options Array
global $of_options;
$of_options = array();

$of_options[] = array( 	"name" 		=> "General Settings",
						"type" 		=> "heading"
				);
$of_options[] = array( 	"name" 		=> "Facebook",
						"desc" 		=> "Enter facebook link",
						"id" 		=> "facebook",
						"std" 		=> "#",
						"type" 		=> "text"
				);
$of_options[] = array( 	"name" 		=> "Twitter",
						"desc" 		=> "Enter twitter link",
						"id" 		=> "twitter",
						"std" 		=> "#",
						"type" 		=> "text"
				);
$of_options[] = array( 	"name" 		=> "Instagram",
						"desc" 		=> "Enter instagram link",
						"id" 		=> "instagram",
						"std" 		=> "#",
						"type" 		=> "text"
				);
$of_options[] = array( 	"name" 		=> "Upload background under the logo",
						"id" 		=> "bg_first",
						"std" 		=> "",
						"type" 		=> "media"
				);
$of_options[] = array( 	"name" 		=> "Upload video",
						"id" 		=> "video_box",
						"std" 		=> "",
						"type" 		=> "media"
				);
$of_options[] = array( 	"name" 		=> "Upload logo",
						"id" 		=> "logo",
						"std" 		=> "",
						"type" 		=> "media"
				);
// $of_options[] = array( 	"name" 		=> "Upload background promo",
// 						"id" 		=> "bg_second",
// 						"std" 		=> "",
// 						"type" 		=> "media"
// 				);
$of_options[] = array( 	"name" 		=> "Video link",
						"id" 		=> "video_link",
						"std" 		=> "https://www.youtube.com/embed/z3biFxZIJOQ",
						"type" 		=> "text"
				);
								
$of_options[] = array( 	"name" 		=> "Tracking Code",
						"desc" 		=> "Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme.",
						"id" 		=> "google_analytics",
						"std" 		=> "",
						"type" 		=> "textarea"
				);
				
$of_options[] = array( 	"name" 		=> "Enter mail",
						"id" 		=> "mail",
						"std" 		=> "info@appraisalscope.com",
						"type" 		=> "text"
				);
				
$of_options[] = array( 	"name" 		=> "Footer Text (copyright)",
						"desc" 		=> "Enter Copyright Text.",
						"id" 		=> "footer_text",
						"std" 		=> "&copy; 2015",
						"type" 		=> "textarea"
				);
				

/* PAGES
$of_options[] = array( 	"name" 		=> "Pages",
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "wrench16.png"
				);
$of_options[] = array( 	"name" 		=> '"Contact" Page',
						"desc" 		=> "Select Page that is used as 'Contact Us'.",
						"id" 		=> "contact_page_id",
						"type" 		=> "radio",
						"options" 	=> $tt_pages
				);
 */
/*			
$of_options[] = array( 	"name" 		=> "Styling Options",
						"type" 		=> "heading"
				);
				
$of_options[] = array( 	"name" 		=> "Custom CSS",
						"desc" 		=> "Quickly add some CSS to your theme by adding it to this block.",
						"id" 		=> "custom_css",
						"std" 		=> "",
						"type" 		=> "textarea"
				);
*/				
$of_options[] = array( 	"name" 		=> "Site Icons",
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "wrench16.png"
				);
				
$of_options[] = array( 	"name" 		=> "Favicon",
						"desc" 		=> "Upload a 16px x 16px image that will represent your website\'s favicon.<br /><br /><em>To ensure cross-browser compatibility, we recommend converting the favicon into .ico format before uploading. (<a href=\"http://www.favicon.cc/\">www.favicon.cc</a>)</em>",
						"id" 		=> "favicon",
						"std" 		=> "",
						"mod"		=> "min",
						"type" 		=> "media"
				);

$of_options[] = array( 	"name" 		=> "Apple Touch Devices Icon 57x57",
						"desc" 		=> "Upload/Select (png) image to be used as webclip icon for apple touch devices. MUST be 57x57 px",
						"id" 		=> "apple_ico",
						"std" 		=> "",
						"mod"		=> "min",
						"type" 		=> "media"
				);

$of_options[] = array( 	"name" 		=> "Apple Touch Devices Icon 72x72",
						"desc" 		=> "Upload/Select (png) image to be used as webclip icon for apple touch devices. MUST be 72x72 px",
						"id" 		=> "apple_ico72",
						"std" 		=> "",
						"mod"		=> "min",
						"type" 		=> "media"
				);

$of_options[] = array( 	"name" 		=> "Apple Touch Devices Icon 114x114",
						"desc" 		=> "Upload/Select (png) image to be used as webclip icon for apple touch devices. MUST be 114x114 px",
						"id" 		=> "apple_ico114",
						"std" 		=> "",
						"mod"		=> "min",
						"type" 		=> "media"
				);



				
// Backup Options
$of_options[] = array( 	"name" 		=> "Backup Options",
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "icon-slider.png"
				);
				
$of_options[] = array( 	"name" 		=> "Backup and Restore Options",
						"id" 		=> "of_backup",
						"std" 		=> "",
						"type" 		=> "backup",
						"desc" 		=> 'You can use the two buttons below to backup your current options, and then restore it back at a later time. This is useful if you want to experiment on the options but would like to keep the old settings in case you need it back.',
				);
				
$of_options[] = array( 	"name" 		=> "Transfer Theme Options Data",
						"id" 		=> "of_transfer",
						"std" 		=> "",
						"type" 		=> "transfer",
						"desc" 		=> 'You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another install, replace the data in the text box with the one from another install and click "Import Options".',
				);
				
	}//End function: of_options()
}//End chack if function exists: of_options()
?>
