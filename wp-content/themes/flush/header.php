<?php
/**
 * The Header for our theme.
 */

//general
global $theme_data;
//social
$facebook = $theme_data['facebook'];
$twitter = $theme_data['twitter'];
$instagram = $theme_data['instagram'];
//google
$google = $theme_data['google_analytics'];
//icons
$favicon = $theme_data['favicon_ico'];
$apple_ico =  $theme_data['apple_ico'];
$apple_ico72 =  $theme_data['apple_ico72'];
$apple_ico114 =  $theme_data['apple_ico114'];
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if ($favicon) {
    ?><link rel="shortcut icon" href="<?= $favicon ?>"><?php } 
    if ($apple_ico) {
    ?><link rel="apple-touch-icon" href="<?= $apple_ico ?>"><?php } 
    if ($apple_ico72) {
    ?><link rel="apple-touch-icon" sizes="72x72" href="<?= $apple_ico72 ?>"><?php } 
        if ($apple_ico114) {
    ?><link rel="apple-touch-icon" sizes="114x114" href="<?= $apple_ico114 ?>"><?php } ?>
    <title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
    <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/style.css">
    <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/style.css">
    <!--[if lt IE 9]>
        <script src="<?php bloginfo( 'template_url' ); ?>/js/html5.js"></script>
        <script src="<?php bloginfo( 'template_url' ); ?>/js/respond.js"></script>
    <![endif]-->
	<?php
		if ( is_singular() && get_option( 'thread_comments' ) )
			wp_enqueue_script( 'comment-reply' );
		wp_head();
	?>
    <?php 
    //Analitycs Code
    if( $tracking_code ){
        echo $tracking_code;
    }
    ?>
</head>
<body class="no-js">
    <div class="main">
        <header>
            <div class="social-header">
                <?php if ($facebook) { ?><a href="<?php echo $facebook; ?>" class="fa fa-facebook"></a><?php } ?>
                <?php if ($twitter) { ?><a href="<?php echo $twitter; ?>" class="fa fa-twitter"></a><?php } ?>
                <?php if ($instagram) { ?><a href="<?php echo $instagram; ?>" class="fa fa-instagram"></a><?php } ?>
            </div><!-- /.social-header -->
            <a href="#" class="open-menu"><em class="fa fa-bars"></em></a>
            <nav>
                <?php 
                    $args = array(
                        'menu_class' => '',
                        'container' => '',
                        'menu' => '(home)'
                    );
                    wp_nav_menu( $args ); 
                ?>
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                <input type="hidden" name="cmd" value="_s-xclick">
                <input type="hidden" name="hosted_button_id" value="SF9JRAEKANE9L">
                <input type="submit" class="paypal" border="0" name="submit" value="DONATE">
                <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                </form>
            </nav> 
        </header>
        <?php query_posts('showposts=1&post_type=partner'); ?>
        <?php while (have_posts()) : the_post(); ?>
        <div class="partners"> 
            <?php if( have_rows('add_partners') ): ?>
            <?php while( have_rows('add_partners') ): the_row(); 
                $image = get_sub_field('upload_image');
            ?>
            <div class="partners-image">
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>">
            </div><!-- /.partners-image -->
            <?php endwhile; ?>
            <?php endif; ?>
        </div><!-- /.partners -->
        <?php endwhile; ?>
        <?php wp_reset_query(); ?> 