<?php
/**
 * The template for displaying the footer.
 */
//general
global $theme_data;
//social
$facebook = $theme_data['facebook'];
$twitter = $theme_data['twitter'];
$instagram = $theme_data['instagram'];
//mail
$mail = $theme_data['mail'];
?>
	
    <footer>
        <div class="wrap">
            <div class="social-footer">
                <?php if ($facebook) { ?><a href="<?php echo $facebook; ?>" class="fa fa-facebook"></a><?php } ?>
                            <?php if ($twitter) { ?><a href="<?php echo $twitter; ?>" class="fa fa-twitter"></a><?php } ?>
                            <?php if ($instagram) { ?><a href="<?php echo $instagram; ?>" class="fa fa-instagram"></a><?php } ?>
            </div><!-- /.social-footer -->
            <?php if (is_front_page()){?>
            <a href="#logo-box" class="footer-promo">watch promo</a>
            <?php } else { ?>
            <a href="/#logo-box" class="footer-promo">watch promo</a>
            <?php } ?>  
            <a href="mailto:<?php echo $mail; ?>" class="contact-link">contact the filmmakers</a>
            <span class="separator-footer"></span>
<a href="https://heartbeat.ua/" class="hb" target="_blank"></a>
        </div><!-- /.wrap -->
    </footer>
    <script src="<?php bloginfo( 'template_url' ); ?>/js/jquery.js"></script>
    <script src="<?php bloginfo( 'template_url' ); ?>/js/library.js"></script>
    <script src="<?php bloginfo( 'template_url' ); ?>/js/script.js"></script>
    <!-- <script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us10.list-manage.com","uuid":"f7df30242cdd62e8a648e8aa4","lid":"ddeb76cb1b"}) })</script> -->
	<?php
		wp_footer();
	?>
</body>
</html>
